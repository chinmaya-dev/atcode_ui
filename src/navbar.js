import React from 'react';
import { NavLink } from "react-router-dom";
import logo from './static/images/atcode_logo.png';
import './App.css';
function Navbar() {
  return (
    <header class="miri-ui-kit-header landing-header header-bg-2">
    <nav class="navbar navbar-expand-lg navbar-dark bg-transparent fixed-on-scroll">
            <div class="container">
                <a class="navbar-brand text-white" href="index.html"><h4 class="text-white">Atcode</h4></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#miriUiKitNavbar"
                    aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="mdi mdi-menu"></span>
                </button>
            
                <div class="collapse navbar-collapse" id="miriUiKitNavbar">
                    <div class="navbar-nav ml-auto">
                        <NavLink to={"/"}>
                        <li class="nav-item">
                            <a class="nav-link" href="">Home</a>
                        </li>
                        </NavLink>
                        <NavLink to={"/Vision"}>
                        <li class="nav-item">
                            <a class="nav-link" href="">Vision</a>
                        </li>
                        </NavLink>
                        <NavLink to={"/Solution"}>
                        <li class="nav-item">
                            <a class="nav-link" href="">Solution</a>
                        </li>
                        </NavLink>
                        <NavLink to={"/Career"}>
                        <li class="nav-item">
                            <a class="nav-link" href="">Career</a>
                        </li>
                        </NavLink>                        
                        <NavLink to={"/Contact"}>
                        <li class="nav-item">
                            <a class="nav-link" href="">Contact</a>
                        </li>
                        </NavLink>
                        
                        
                    </div>
                </div>
            </div>
        </nav>
        <div
            class="miri-ui-kit-header-content text-center text-white d-flex flex-column justify-content-center align-items-center">
            <h1 class="display-3 text-white">AtCode</h1>
            <p class="h3 font-weight-light text-white">Software strategy consultants</p>
        </div>
        </header>
  );
}
export default Navbar;