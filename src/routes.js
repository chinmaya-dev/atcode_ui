import React from 'react';
import Home from './Home';
import Vision from './Vision';
import NavBar from './navbar';
import { Route, Switch, Redirect } from 'react-router-dom';

export const Routes = () => {
  return (
    <div>
      <NavBar />
      <Switch>
        <Route exact path="/Home" component={Home} />
        <Route exact path="/">
          <Redirect to="/Home" />
        </Route>
        <Route exact path="/Vision" component={Vision} />
      </Switch>
    </div>
  );
};