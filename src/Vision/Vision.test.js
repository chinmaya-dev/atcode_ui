import React from "react";
import { shallow } from "enzyme";
import Vision from "./Vision";

describe("Vision", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Vision />);
    expect(wrapper).toMatchSnapshot();
  });
});
