import React, { Component } from "react";
import visionimage from '../static/images/our_vision_image.png';
class Vision extends Component {
  render() {
    return (
      <div class="container content-wrapper" id="demo-content">
        <div class="card-body mb-4">
          <div class="row">
            <div class="col-md-6">

              <h2 class="card-title">Our Vision</h2>
              <h5>Embrace the revolution of intelligent software engineering and design.</h5>
              <p>Within Atcode App,
                We specialize in delivering industry specific software solutions, strategic
                implementation, and integration services to arrays of organizations.
                Collaborating closely with our clients to provide customized solutions that
                yield concrete outcomes.
                We have a team of certified professionals who possess a deep expertise in
                the platform and provide a comprehensive
                set of services including Software Implementations and Integration,
                Business Analytics and Intelligence and Operation Management.
                If you are in the market for tailored and budget-friendly comprehensive
                services to meet the requirements of diverse Tech organizations
                we are the preferred partner. Our offerings encompass personalized, cost
                efficient end-to-end solutions, delivers customized services
                that cover the entire spectrum of technology infrastructure. </p>
            </div>
            <div class="col-md-6">
              <img alt="atcode_vision_image" src={visionimage}></img>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Vision;
