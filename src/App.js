import React from "react";
import logo from './logo.svg';
import './App.css';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import Home from "./Home";
import Vision from "./Vision";
import Navbar from "./navbar";
import Solution from "./Solution";
import Career from "./Career";
import Contact from "./Contact";
import Footer from "./footer";

function App() {
  return (
    <div className="App">
      <Navbar />
       <Routes>
         <Route>
           <Route path="/" element={<Home />} />
           <Route path="Vision" element={ <Vision /> } />
           <Route path="Solution" element={ <Solution /> } />
           <Route path="Career" element={ <Career /> } />
           <Route path="Contact" element={ <Contact /> } />
         </Route>
       </Routes>
       <Footer />
    </div>
  );
}

export default App;
