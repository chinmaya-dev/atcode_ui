import React, { Component,useEffect } from "react";

import { useState } from 'react';

class Contact extends Component {
 state={
  name:"",
  company:"",
  email:"",
  phone:"",
  message:"",
  visible:""
 }
 onChange = e =>{
  this.setState({[e.target.name]:e.target.value});
 }
 onSubmit = async (e) =>{
  e.preventDefault();
  const parameter = {
    "parameter":{
      "name": this.state.name,
      "company": this.state.company,
      "email": this.state.email,
      "phone": this.state.phone,
      "message": this.state.message
    }    
  }
  try {
    let res = await fetch("http://localhost:8000/api/v1/create_contact", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(parameter)
    });
    let resJson = await res.json();
    if (res.status === 200) {
      this.setState({visible:true});
      setTimeout(() => {
        this.setState({visible:false});
      }, 10000)
    } else {
      console.log("Some error occured");
    }
  } catch (err) {
    console.log(err);
  }
 }
  render() {
    const {name,company,email,phone,message,visible} = this.state;
    return (
      
      <section class="miri-ui-kit-section contact-section">
        <div class="container">
          <h2 class="card-title d-flex justify-content-center">Contact us</h2>
          <div class="row d-flex justify-content-center">
            <form onSubmit={this.onSubmit}>
              <div class="row">
                <div class="form-group col-md-6">
                  <input type="text" name='name' value={name} onChange={this.onChange} class="form-control form-control-pill" placeholder="Name" />
                </div>

                <div class="form-group col-md-6">
                  <input type="text" name='company' value={company} onChange={this.onChange} class="form-control form-control-pill" placeholder="Company" />
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <input type="text" name='phone' value={phone} onChange={this.onChange} class="form-control form-control-pill" placeholder="Phone" />
                </div>
                <div class="form-group col-md-6">
                  <input type="text" name='email' value={email} onChange={this.onChange} class="form-control form-control-pill" placeholder="Email" />
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-12">
                  <textarea name='message' value={message} onChange={this.onChange} class="form-control form-control-pill" placeholder="Message" />
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                  <input type="submit" value="Submit" class="btn btn-block btn-pill btn-primary" />
                </div>
              </div>
              <div class="card-body mb-4" style={{display: visible ? 'block' : 'none'}}>
                <div class="row">
                  <div class="col-md-12">                    
                    <div class="alert alert-success" role="alert">
                      Message sent successfully
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i aria-hidden="true" class="mdi mdi-close"></i>
                      </button>
                    </div>
                  </div>
                  
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>



      
    );
  }
}

export default Contact;
