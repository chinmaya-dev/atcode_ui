import React, { Component } from "react";

import axios from 'axios';

class Career extends Component {
  state = {
    firstname: "",
    lastname: "",
    email: "",
    phone: "",
    selectedFile: null,
    visible: ""
  }
  onFileChange = event => {

    // Update the state
    this.setState({ selectedFile: event.target.files[0] });

  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }
  onSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append(
      "firstname",
      this.state.firstname
    );
    formData.append(
      "lastname",
      this.state.lastname
    );
    formData.append(
      "email",
      this.state.email
    );
    formData.append(
      "phone",
      this.state.phone
    );
    // Update the formData object
    formData.append(
      "file",
      this.state.selectedFile
    );


    try {
      axios({
        method: "post",
        url: "http://localhost:8000/api/v1/create_career",
        data: formData,
        headers: { "Content-Type": "multipart/form-data" },
      })
        .then(function (response) {
          this.setState({ visible: true });
          setTimeout(() => {
            this.setState({ visible: false });
          }, 10000)
        })
        .catch(function (response) {
          //handle error
          console.log(response);
        });

    } catch (err) {
      console.log(err);
    }
  }
  render() {
    const { firstname, lastname, email, phone, selectedFile, visible } = this.state;
    return (
      <div class="container content-wrapper" id="demo-content">
        <div class="card-body mb-4">
          <div class="row">
            <div class="col-md-6">

              <h2 class="card-title">Careers within Atcode App</h2>
              <h5>Embrace the revolution of intelligent software engineering and design.</h5>
              <p>Atcode App is continuously seeking exceptional
                individuals to become part of our team. As experts in
                software technology, we provide a diverse set of services
                encompassing implementation, customization,
                integration, and training. If you possess a strong
                enthusiasm for empowering businesses to thrive through
                the potential of technology, we eagerly await your
                application. </p>
              <p>We offer attractive compensation packages and
                comprehensive benefits, along with ample opportunities
                for professional growth and advancement. Discover the
                diverse career opportunities available at Atcpde App and
                join a team committed to facilitating the success of our
                clients
              </p>
            </div>
            <div class="col-md-6">
              <section class="miri-ui-kit-section contact-section">
                <div class="container">
                  <div class="row">
                    <div class="col-md-6 offset-md-3">
                      <form onSubmit={this.onSubmit}>
                        <div class="row">
                          <div class="form-group col-md-12">
                            <input type="text" name='firstname' value={firstname} onChange={this.onChange} class="form-control form-control-pill" placeholder="First Name" />
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                            <input type="text" name='lastname' value={lastname} onChange={this.onChange} class="form-control form-control-pill" placeholder="Last Name" />
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                            <input type="text" name='phone' value={phone} onChange={this.onChange} class="form-control form-control-pill" placeholder="Phone" />
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                            <input type="text" name='email' value={email} onChange={this.onChange} class="form-control form-control-pill" placeholder="Email" />
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                            <p>Upload your resume here:</p>
                            <input type="file" name='selectedFile' onChange={this.onFileChange} />
                          </div>
                        </div>

                        <div class="row">
                          <div class="form-group col-md-12">
                            <input type="submit" value="Submit" class="btn btn-block btn-pill btn-primary" />
                          </div>
                        </div>
                        <div class="card-body mb-4" style={{ display: visible ? 'block' : 'none' }}>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="alert alert-success" role="alert">
                                Profile added successfully
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <i aria-hidden="true" class="mdi mdi-close"></i>
                                </button>
                              </div>
                            </div>

                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </section>

            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default Career;
