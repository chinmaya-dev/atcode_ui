import React from "react";
import { shallow } from "enzyme";
import Career from "./Career";

describe("Career", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Career />);
    expect(wrapper).toMatchSnapshot();
  });
});
