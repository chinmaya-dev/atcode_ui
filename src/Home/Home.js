import React, { Component } from "react";
import { Link } from "react-router-dom";
import workimage from '../static/images/count-card-bg.jpg';

class Home extends Component {
  render() {
    return (
      <div>
      <section class="miri-ui-kit-section how-we-work-section mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h6 class="text-primary mb-3">HOW WE WORK</h6>
                        <h2 class="h1 font-weight-semibold mb-4">Embrace the revolution of intelligent software engineering and design.
                        </h2>
                        <p class="mb-5">We specialize in delivering industry specific software solutions, strategic
                        implementation, and integration services to arrays of organizations.
                        Collaborating closely with our clients to provide customized solutions that
                        yield concrete outcomes.</p>
                        <p><button class="btn btn-rounded btn-primary raise-on-hover" data-toggle="lightbox" data-target="#demoVideoLightbox"><i class="mdi mdi-play"></i></button><span
                                class="button-label text-primary font-weight-medium ml-3">SEE HOW WE WORKS</span></p>
                </div>
                <div class="col-md-6 d-md-flex justify-content-end">
                    <div class="card bg-dark text-white count-card">
                        <img src={workimage} alt="about 1" class="card-img" />
                        <div class="card-img-overlay">                            
                            <div class="count-box bg-primary">
                                <span class="h2 text-white">100+</span>
                                <span class="font-weight-medium">Customers</span>
                            </div>                            
                            <div class="count-box bg-primary">
                                <span class="h2 text-white">50+</span>
                                <span class="font-weight-medium">Clients</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="miri-ui-kit-section mt-5">
    <div class="container">
      <div class="card-body mb-4">
        <div class="row">
          <div class="col-md-12">

            <h2 class="card-title text-center">Our Solutions</h2>
          </div>
        </div>
      </div>
      <div class="d-md-flex justify-content-between row">

        <div class="feature-box px-3 col-md-3">
          <span class="card-icon bg-primary text-white rounded-circle"><i class="mdi mdi-bell"></i></span>
          <h4 class="mb-3">Agile Beginnings</h4>
          <p>Address your fundamental tech needs promptly and efficiently through our Agile kickoffs. Atcode presents cloud solutions and Key merits of opting for a Agile Beginnings.</p>
        </div>


        <div class="feature-box px-3 col-md-3">
          <span class="card-icon bg-primary text-white rounded-circle"><i
            class="mdi mdi-laptop"></i></span>
          <h4 class="mb-3">Technology implementation</h4>
          <p>Deploying software can be a challenging and intimidating endeavor. At Atcode, our objective is to simplify and streamline your implementation journey.</p>
        </div>


        <div class="feature-box px-3 col-md-3">
          <span class="card-icon bg-primary text-white rounded-circle"><i class="mdi mdi-comment-question-outline"></i></span>
          <h4 class="mb-3">Business Analytics & Intelligence</h4>
          <p>Our Business Analytics and Intelligence services empower organizations to stay ahead of the competition in today's data driven business landscape.</p>
        </div>

        <div class="feature-box px-3 col-md-3">
          <span class="card-icon bg-primary text-white rounded-circle"><i class="mdi mdi-vibrate"></i></span>
          <h4 class="mb-3">Integration</h4>
          <p>With our extensive experience and proficiency, AtCode is well equipped to help your company establish connections</p>
        </div>

      </div>
    </div>
  </section>
  </div>
    );
  }
}

export default Home;
