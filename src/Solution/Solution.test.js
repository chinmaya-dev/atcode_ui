import React from "react";
import { shallow } from "enzyme";
import Solution from "./Solution";

describe("Solution", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Solution />);
    expect(wrapper).toMatchSnapshot();
  });
});
