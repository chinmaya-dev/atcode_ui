import React from 'react';
import { NavLink } from "react-router-dom";
import './App.css';
function Footer() {
  return (
    <footer>
        <div class="container">
            <nav class="navbar navbar-dark navbar-expand d-block d-sm-flex text-center">
                <span>Email:info@atcode.app Phone:(111)-222-3333</span>
                <div class="navbar-nav ml-auto justify-content-center">
                    <button class="btn bg-primary text-white">Contact Us</button>
                </div>
            </nav>
        </div>
    </footer>
  );
}
export default Footer;